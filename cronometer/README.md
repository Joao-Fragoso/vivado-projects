# Cronometer

A full Vivado project (sources and constraints) for a Cronometer to run on 
Digilent Nexys 4 DDR (renamed now to Nexys A7).
Source codes are commented, but feel free to send any questions.

Cronometer uses the eight displays on board to present current time in HH.MM.SS.TT format 
(HH hours, MM minutes, SS seconds and TT tenths of sceonds).
LED[0] on the board is power indicator
LED[6..15] runs with cronometer, shifting each tenth of seconds. They are off when cronometer
is sttoped.

SW[0] is a global reset active low.

Button UP is cronometer restart
Button DOWN is cronometer start/stop
Button RIGHT is cronometer lap function (hold value on display but not stop cronometer)

Project created and tested on Xilinx Vivado 2017.3.
Some warning on 2017.3 are expecteds. You should ignore warnings [Synth 8-6014], since
it is a bug in 2017.3 as stated in https://forums.xilinx.com/t5/Synthesis/Many-spurious-quot-Synth-8-6014-Unused-sequential-element-was/td-p/769636#.XVncqQcpXaA.gmail.
There is one(double) warning [Synth 8-3917] on led_pwr signal, since it is always on, but it
is the intetion.
Also a Implementation warning from missing CONFIG_VOLTAGE. It appears not to be a problem, but we expect
to solve this in constraints file. 

If you get futher errors and/or warnings, please let we know.

### Before run
1. Download vivado-boards-master.zip from Git Hub root
2. Uncompress this zip file
3. Copy all folders from vivado-boards-master/new/board_files to ${Xilinx}/Vivado/${version}/data/boards/board_files

### How to run
1. Download (or clone) this directory
2. Launch Vivado
3. On start screen, use tcl console commands to navigate to your own copy 
4. source cronometer.tcl
5. Generate bitstream and download it to a Nexys board.


