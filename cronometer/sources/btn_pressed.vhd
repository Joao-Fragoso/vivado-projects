----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: btn_pressed - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This a simple filter for detecting button pressed.
--           Create a one clock cycle pulse every time the button is pressed.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity btn_pressed is
	generic(
		NR_BUTTONS : integer := 1 -- nro of buttons to filter
	);
    port ( 
		clk     : in  std_logic; -- clock signal
		rst_n   : in  std_logic; -- global synchronous reset
		btn     : in  std_logic_vector(0 to NR_BUTTONS-1); -- input button
		pressed : out std_logic_vector(0 to NR_BUTTONS-1) -- button pressed indicating
	);
end btn_pressed;

architecture rtl of btn_pressed is
	signal delayed_btn : std_logic_vector(0 to NR_BUTTONS-1);	
begin

	process(clk)
	begin
		if (clk'event and clk='1') then
			if (rst_n = '0') then
				delayed_btn <= (others => '0');
			else
				delayed_btn <= btn;
			end if;
		end if;
	end process;

	pressed <= btn and (not delayed_btn);
	
end rtl;
