----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: components (package)
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: Package with a set of reusable components.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package components is 

    procedure reg(
        signal clk      : in  std_logic;
        signal data_in  : in  std_logic_vector;
        signal data_out : out std_logic_vector
    );

    procedure reg_w_enable(
        signal clk      : in  std_logic;
        signal enable   : in  std_logic;
        signal data_in  : in  std_logic_vector;
        signal data_out : out std_logic_vector
    );

    component fifo
	generic (
		FIFO_DEPTH : integer := 3
	);
    port ( 
		clk      : in  std_logic; -- clock signal
		async_in : in  std_logic; -- async input
		sync_out : out std_logic  -- synchronized output
	);
    end component fifo;

    component btn_pressed
        generic(
            NR_BUTTONS : integer := 1 -- nro of buttons to filter
        );
        port ( 
            clk     : in  std_logic; -- clock signal
            rst_n   : in  std_logic; -- global synchronous reset
            btn     : in  std_logic_vector(0 to NR_BUTTONS-1); -- input button
            pressed : out std_logic_vector(0 to NR_BUTTONS-1) -- button pressed indicating
        );
    end component btn_pressed;
    
    component seven_segments
        port ( 
            clk     : in  std_logic; -- clock signal
            rst_n   : in  std_logic; -- synchronous reset active low
            value   : in  std_logic_vector(31 downto 0); -- hex data to display (4 bits per display)
            point   : in  std_logic_vector(7 downto 0);  -- point control
            an      : out std_logic_vector(7 downto 0); -- display anode selection
            segment : out std_logic_vector(7 downto 0) -- segments control
        );
    end component seven_segments;

    component delay_10ms
        generic (
            CLK_FREQ : integer := 100_000_000 -- clock frequency
        );
        port ( 
            clk        : in  std_logic; -- clock signal
            rst_n      : in  std_logic; -- synchronous reset active low
            pulse100Hz : out std_logic  -- pulse every 10 ms;
        );
    end component delay_10ms;

    component counter
        generic (
            SIZE   : integer := 4; -- number of bits for the output counter (MODULO <= 2**SIZE)
            MODULo : integer := 10 -- counter modulos. Counter will count from 0 to MODULO-1
            );
        port ( 
            clk    : in  std_logic; -- clock signal
            rst_n  : in  std_logic; -- synchronous reset active low
            enable : in  std_logic; -- counter enable
            value  : out std_logic_vector(SIZE-1 downto 0); -- current counter value
            tc     : out std_logic  -- terminal counter
        );
    end component counter;

    component cronometer_counter
        port ( 
            clk        : in  std_logic; -- clock signal
            rst_n      : in  std_logic; -- synchronous reset active low
            enable     : in  std_logic; -- cronometer enable
            pulse100Hz : in  std_logic; -- one clock cycle pulse every 10ms
            value      : out std_logic_vector(31 downto 0); -- current timer value
            dots       : out std_logic_vector(7 downto 0)
        );
    end component cronometer_counter;

end package components;

package body components is

    procedure reg(
        signal clk      : in  std_logic;
        signal data_in  : in  std_logic_vector;
        signal data_out : out std_logic_vector
    ) is
    begin
        if (clk'event and clk = '1') then
            data_out <= data_in;
        end if;
    end procedure reg;

    procedure reg_w_enable(
        signal clk      : in  std_logic;
        signal enable   : in  std_logic;
        signal data_in  : in  std_logic_vector;
        signal data_out : out std_logic_vector
    ) is
    begin
        if (clk'event and clk = '1') then
            if (enable = '1') then
                data_out <= data_in;
            end if;
        end if;
    end procedure reg_w_enable;

end package body components;