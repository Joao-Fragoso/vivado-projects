----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: counter - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This is parametric counter. It will be used in a BCD counter
--          for the cronometer.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity counter is
	generic (
        SIZE   : integer := 4; -- number of bits for the output counter (MODULO <= 2**SIZE)
        MODULO : integer := 10 -- counter modulos. Counter will count from 0 to MODULO-1
	);
    port ( 
		clk    : in  std_logic; -- clock signal
        rst_n  : in  std_logic; -- synchronous reset active low
        enable : in  std_logic; -- counter enable
        value  : out std_logic_vector(SIZE-1 downto 0); -- current counter value
        tc     : out std_logic  -- terminal counter
	);
end counter;

architecture rtl of counter is
    signal current_value : std_logic_vector(SIZE-1 downto 0);
begin

    process(clk)
    begin
        if (clk'event and clk = '1') then
            if (rst_n = '0') then
                current_value <= (others => '0');
            else
                if (enable = '1') then
                    current_value <= current_value + 1;
                    if (current_value = (MODULO-1)) then
                        current_value <= (others => '0');
                    end if;
                end if;
            end if;
        end if;
    end process;

    value <= current_value;

    tc <= '1' when (enable = '1' and (current_value = (MODULO-1))) else '0';

end rtl;
