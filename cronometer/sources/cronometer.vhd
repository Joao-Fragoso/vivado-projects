----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: cronometer - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: A simpler cronometer for Digilent Nexyx 4 DDR Board.
--		    This project can be synthesized and it can be downloaded to a Nexys 4
--           DDR board
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.components.all;

entity cronometer is
    port ( 
		clk100MHz  : in  std_logic; -- clock signal
		rst_n      : in  std_logic; -- reset (active low) for state machine
		restart    : in  std_logic; -- restart cronometer
		start_stop : in  std_logic; -- start/stop cronometer button
		lap        : in  std_logic; -- lap button (hold display not cronometer)
		segment    : out std_logic_vector(7 downto 0); --seven segments + dot output
		an		   : out std_logic_vector(7 downto 0); -- anodo displays control
		led_pwr	   : out std_logic;  -- power on led
		led        : out std_logic_vector(9 downto 0) -- led pulse at seconds
	);
end cronometer;

architecture rtl of cronometer is
	signal sync_restart : std_logic;
	signal sync_start_stop : std_logic;
	signal sync_lap : std_logic;
	signal start_stop_pressed : std_logic;
	signal lap_pressed : std_logic;
	signal running : std_logic;
	signal hold : std_logic;
	signal restart_crono_n : std_logic;
	signal cronometer_value : std_logic_vector(31 downto 0);
	signal cronometer_hold  : std_logic_vector(31 downto 0);
	signal clk100Hz : std_logic;
	signal dots : std_logic_vector(7 downto 0);
	type crono_states_type is (IDLE_STATE, STOP_STATE, RUN_STATE, LAP_STATE);
	signal crono_state : crono_states_type;
    signal led_run : std_logic_vector(9 downto 0);
begin

	f0 : fifo generic map (FIFO_DEPTH => 3) 
			  port map (clk=>clk100MHz, async_in => restart, sync_out => sync_restart);
			  
	f1 : fifo generic map (FIFO_DEPTH => 3) 
			  port map (clk=>clk100MHz, async_in => start_stop, sync_out => sync_start_stop);
			  
	f2 : fifo generic map (FIFO_DEPTH => 3) 
			  port map (clk=>clk100Mhz, async_in => lap, sync_out => sync_lap);
	
	press_filter : btn_pressed
		generic map ( NR_BUTTONS => 2)
		port map (
			clk => clk100MHz,
			rst_n => rst_n,
			btn(1) => sync_start_stop,
			btn(0) => sync_lap,
			pressed(1) => start_stop_pressed,
			pressed(0) => lap_pressed
		);
	
	pulse_generator : delay_10ms
		generic map (
			CLK_FREQ => 100_000_000
		)
		port map (
			clk => clk100MHz,
			rst_n => rst_n,
			pulse100Hz => clk100Hz
		);

	crono : cronometer_counter
		port map (
			clk => clk100MHz,
			rst_n => restart_crono_n,
			enable => running,
			pulse100Hz => clk100Hz,
			value => cronometer_value,
			dots => dots
		);

	hold_process : process(clk100MHz)
	begin
		if (clk100MHz'event and clk100MHz = '1') then
			if (hold = '0') then
				cronometer_hold <= cronometer_value;
			end if;
		end if;
	end process;

	display_control : seven_segments
		port map (
			clk => clk100MHz,
			rst_n => rst_n,
			value => cronometer_hold,
			point => dots,
			an => an,
			segment => segment
		);

	-- always on
	led_pwr <= '1';

	-- will blink at 1Hz when running
	with cronometer_value(7 downto 4) select led_run <=
	        "1000000000" when x"9",
	        "0100000000" when x"8",
	        "0010000000" when x"7",
	        "0001000000" when x"6",
	        "0000100000" when x"5",
	        "0000010000" when x"4",
	        "0000001000" when x"3",
	        "0000000100" when x"2",
	        "0000000010" when x"1",
	        "0000000001" when others;

    led <= led_run when running = '1' else "0000000000";
    
	running <= '1' when (crono_state = RUN_STATE or crono_state = LAP_STATE) else '0';
	hold <= '1' when crono_state = LAP_STATE else '0';
	restart_crono_n <= '0' when crono_state = IDLE_STATE else '1';

	main_ctrl : process(clk100Mhz)
	begin
		if (clk100MHz'event and clk100MHz='1') then
			if (rst_n ='0') then
				crono_state <= IDLE_STATE;
			else
				case crono_state is
				when STOP_STATE =>
					if (sync_restart = '1') then
						crono_state <= IDLE_STATE;
					elsif (start_stop_pressed='1') then
						crono_state <= RUN_STATE;
					end if;
				when RUN_STATE =>
					if (sync_restart = '1') then
						crono_state <= IDLE_STATE;
					elsif (start_stop_pressed='1') then
						crono_state <= STOP_STATE;
					elsif (lap_pressed = '1') then
						crono_state <= LAP_STATE;
					end if;
				when LAP_STATE =>
					if (sync_restart = '1') then
						crono_state <= IDLE_STATE;
					elsif (start_stop_pressed='1') then
						crono_state <= STOP_STATE;
					elsif (lap_pressed = '1') then
						crono_state <= RUN_STATE;
					end if;
				when others => -- IDLE
					if (sync_restart = '0') then
						crono_state <= STOP_STATE;
					end if;
				end case;
			end if;
		end if;
	end process;

end rtl;
