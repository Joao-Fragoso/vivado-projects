----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: counter - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This is parametric counter. It will be used in a BCD counter
--          for the cronometer.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
library work;
use work.components.all;

entity cronometer_counter is
    port ( 
		clk        : in  std_logic; -- clock signal
        rst_n      : in  std_logic; -- synchronous reset active low
        enable     : in  std_logic; -- cronometer enable
        pulse100Hz : in  std_logic; -- one clock cycle pulse every 10ms
        value      : out std_logic_vector(31 downto 0); -- current timer value
        dots       : out std_logic_vector(7 downto 0)
	);
end cronometer_counter;

architecture rtl of cronometer_counter is
    signal tc : std_logic_vector(6 downto 0);
    signal count_enable : std_logic;
begin

    dots <= "10101011"; -- dots separators...
    count_enable <= enable and pulse100Hz;

    c0 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => count_enable,
        value => value(3 downto 0),
        tc => tc(0)
    );

    c1 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(0),
        value => value(7 downto 4),
        tc => tc(1)
    );

    c2 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(1),
        value => value(11 downto 8),
        tc => tc(2)
    );

    c3 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 6
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(2),
        value => value(15 downto 12),
        tc => tc(3)
    );

    c4 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(3),
        value => value(19 downto 16),
        tc => tc(4)
    );

    c5 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 6
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(4),
        value => value(23 downto 20),
        tc => tc(5)
    );

    c6 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(5),
        value => value(27 downto 24),
        tc => tc(6)
    );

    c7 : counter 
    generic map (
        SIZE   => 4,
        MODULO => 10
    )
    port map (
        clk => clk,
        rst_n => rst_n,
        enable => tc(6),
        value => value(31 downto 28),
        tc => open
    );

end rtl;
