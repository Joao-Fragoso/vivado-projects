----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: data_shifter
-- Module Name: data_shifter - rtl
-- Project Name: Data Shifter
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This a simple data storage, shifter and display.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity data_shifter is
    port ( 
		clk     : in  std_logic; -- clock signal
		rst_n   : in  std_logic; -- reset (active low) for state machine
        push_in : in  std_logic; -- push a new data in
        value   : in  std_logic_vector(3 downto 0); -- a hex value to push in
		segment : out std_logic_vector(7 downto 0); --seven segments + dot output
		an		: out std_logic_vector(7 downto 0); -- anodo displays control
		led_pwr : out std_logic  -- power on led
	);
end data_shifter;

architecture rtl of data_shifter is
    signal data : std_logic_vector(31 downto 0);
    signal push_button : std_logic_vector(0 to 3);
    signal push : std_logic;

    component seven_segments
        port ( 
            clk     : in  std_logic; -- clock signal
            rst_n   : in  std_logic; -- synchronous reset active low
            value   : in  std_logic_vector(31 downto 0); -- hex data to display (4 bits per display)
            point   : in  std_logic_vector(7 downto 0);  -- point control
            an      : out std_logic_vector(7 downto 0); -- display anode selection
            segment : out std_logic_vector(7 downto 0) -- segments control
        );
    end component seven_segments;

begin

    led_pwr <= '1';

    process(clk)
    begin
        if (clk'event and clk='1') then
            push_button <= push_in & push_button(0 to 2); -- shift button to filter
        end if;
    end process;
    
    push <= not push_button(3) and push_button(2);

    process(clk)
    begin
        if (clk'event and clk='1') then
            if (rst_n = '0') then
                data <= (others => '0');
            else
                if (push='1') then
                    data <= data(27 downto 0) & value;
                end if;
            end if;
        end if;
    end process;

    display : seven_segments
    port map (
        clk => clk,
        rst_n => rst_n,
        value => data,
        point => "01010101",
        an => an,
        segment => segment
    );

end rtl;
