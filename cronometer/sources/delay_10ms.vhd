----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: delay_10ms - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This is a delay counter. Sends a pulse every 10ms.
--          It is used to counter time in cronometer.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

entity delay_10ms is
    generic (
        CLK_FREQ : integer := 100_000_000 -- clock frequency
    );
    port ( 
		clk        : in  std_logic; -- clock signal
		rst_n      : in  std_logic; -- synchronous reset active low
		pulse100Hz : out std_logic  -- pulse every 10 ms;
	);
end delay_10ms;

architecture rtl of delay_10ms is
    constant CYCLES : integer := (CLK_FREQ/100);
    constant NR_BITS : integer := integer(ceil(log2(real(CYCLES)))) ;
    signal   counter : std_logic_vector(NR_BITS-1 downto 0) ; -- counter delay
begin

    process(clk)
    begin
        if (clk'event and clk = '1') then
            if (rst_n = '0') then
                counter <= (others => '0');
            else
                counter <= counter + 1;
                if (counter = (CYCLES-1)) then  -- 999,999 cycles (10 ms @ 100MHz)
                    counter <= (others => '0');
                end if;
            end if;
        end if;
    end process;
    
    pulse100Hz <= '1' when (counter = (CYCLES-1)) else '0';

end rtl;