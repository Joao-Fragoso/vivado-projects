----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 08/15/2019 15:00:00
-- Design Name: cronometer
-- Module Name: fifo - rtl
-- Project Name: Cronometer
-- Target Devices: Nexys 4 DDR (xc7a100tcsg324-1) Digilent Board
-- Tool Versions: Vivado 2017.3
-- Description: This a simple parametric fifo for synchronozing asynchronous
--				signals.
-- Dependencies: Board information on Vivado instalation
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity fifo is
	generic (
		FIFO_DEPTH : integer := 3
	);
    port ( 
		clk      : in  std_logic; -- clock signal
		async_in : in  std_logic; -- async input
		sync_out : out std_logic  -- synchronized output
	);
end fifo;

architecture rtl of fifo is
	signal fifo_s : std_logic_vector(1 to FIFO_DEPTH);
	
begin
	-- process for registering input X
	fifo_reg : process(clk)
	begin
		if (clk'event and clk='1') then
			fifo_s <= async_in & fifo_s(1 to FIFO_DEPTH-1);
		end if;
	end process;
	
	-- output assignement (Vhdl'94)
	sync_out <= fifo_s(FIFO_DEPTH);

end rtl;
