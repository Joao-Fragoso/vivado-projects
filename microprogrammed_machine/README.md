# Microprogrammed Machine

A full Vivado project (sources and simulation) for a Microprogrammed machine as described in
chapter 14 of [Introduction to Digital System](https://www.amazon.com/Introduction-Digital-Systems-Milos-Ercegovac/dp/0471527998) 
of Milos Ercegovac, Tomas Lang and Jaim Moreno

Source codes are commented, but for more details, please refer to book.

A video explanaing the code is avaiable at https://www.youtube.com/watch?v=l77VpEQCAWA&t=2057s (Brazilian portuguese narration).

Project created and tested on Xilinx Vivado 2017.3.

### How to run
1. Download (or clone) this directory
2. Launch Vivado
3. On start screen, use tcl console commands to navigate to your own copy 
4. source project_init.tcl
