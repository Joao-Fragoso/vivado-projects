----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/20/2019 13:30:00 PM
-- Design Name: tb_machine
-- Module Name: tb_machine - rtl
-- Project Name: microprogrammed_machine
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: Testbench for simulating microprogrammed machine with counter one program.
-- 				Microprogrammed controller with datapath.
--              Based on chap 14 - Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to chapter 14.
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
--		CHANGE EXPECTED VALUE IF YOU CHANGE THE PROGRAM!!
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;

-- empty entity
entity tb_machine is
end tb_machine;


architecture behavioral of tb_machine is

	-- component declaration
	component top is
	port (
		clk   : in  std_logic; -- clock signal
		start : in  std_logic; -- start (active high) start the machine
		rst_n : in  std_logic; -- reset (active low) global
		x     : in  std_logic_vector (7 downto 0);  -- input data
		z     : out std_logic_vector (7 downto 0); -- output data
		done  : out std_logic -- programs ends signal
	);  
	end component top;

	-- input signals
	signal clk : std_logic := '0';
	signal start : std_logic := '0';
	signal rst_n : std_logic := '0';
	signal x : std_logic_vector (7 downto 0);
	-- output signals
	signal z : std_logic_vector (7 downto 0);
	signal done : std_logic;
	-- testbench signals
	signal expected : std_logic_vector(7 downto 0);
	type CHECK_TYPE is (WAITING, OK, FAIL);
	signal check : CHECK_TYPE;

begin
	-- machine instantiation
	machine : top port map (
		clk   => clk,
		start => start,
		rst_n => rst_n,
		x     => x,
		z     => z,
		done  => done
	);
	
	-- clock generation
	clock_gen : clk <= not clk after 5 ns; -- generating 10ns period clock

	-- verifying result
	verify : check <= OK   when (start = '0' and done = '1' and expected = z) else
	                  FAIL when (start = '0' and done = '1' and expected /= z) else
	                  WAITING;
	         
	process
	begin
		wait for 25 ns;  -- wait for reset
		rst_n <= '1';  -- de assert reset signal
		-- wait for five clock periods.
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		-- start program
		start <= '1';
		x <= "10110111";
		expected <= x"06";
		-- wait for done be de-asserted
		wait until done <= '0';
		-- wait one clock period and synchronize
		wait until clk'event and clk='1';
		-- de-assert start
		start <= '0';
		-- wait for program end and done be asserted again
		wait until done <= '1';
		-- wait a new clock cycle
		wait until clk'event and clk='1';

		-- start a new computation (program execution)
		start <= '1';
        x <= "10000111";
        expected <= x"04";
		wait until done <= '0';
		wait until clk'event and clk='1';
		start <= '0';
		wait until done <= '1';
        wait until clk'event and clk='1';
		
		--
		-- stop simulation with a failure urghh!!! :(
		assert false report "Simualtion ends!!!" severity failure;
	end process;

end behavioral;
