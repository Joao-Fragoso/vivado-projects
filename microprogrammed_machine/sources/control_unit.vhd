----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/20/2019 13:30:00 PM
-- Design Name: control_unit
-- Module Name: control_unit - rtl
-- Project Name: microprogrammed_machine
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: Microprogrammed controller with datapath.
--              Based on chap 14 - Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to chapter 14.
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity control_unit is
    port ( 
        clk    : in  std_logic;
        rst_n  : in  std_logic;
        start  : in  std_logic;
        done   : out std_logic;
        fld_a  : out std_logic_vector (2 downto 0);
        fld_b  : out std_logic_vector (2 downto 0);
        fld_c  : out std_logic_vector (2 downto 0);
        sel_x  : out std_logic;
        ld_x   : out std_logic;
        ld_z   : out std_logic;
        ld_reg : out std_logic;
        alu_op : out std_logic_vector (1 downto 0);
        cy     : in  std_logic;
        neg    : in std_logic;
        zr     : in  std_logic
	);
end control_unit;

architecture rtl of control_unit is
	-- current program address
	signal csar : std_logic_vector(5 downto 0);
	-- type for further ROM declaration
	type rom_type is array(natural range <>) of std_logic_vector(17 downto 0);
	-- ROM declaration
	-- Initialize with a program to count number of ones in input X
	-- for more details, please refer chapter 14
	constant rom : rom_type(0 to 31) := (0  => "001000000000100010",
									     1  => "100000000000000001",
										 2  => "011000000011110001",
										 3  => "000000000010100100",
										 4  => "000000000111100000",
										 5  => "000010010010100000",
										 6  => "111000000000001000",
										 7  => "011111000111100000",
										 8  => "000011011011100000",
										 9  => "111000000000000101",
										 10 => "000111000111101000",
										 11 => "111000000000000000",
										 12 => "000000000000000000",
										 13 => "000000000000000000",
										 14 => "000000000000000000",
										 15 => "000000000000000000",
										 16 => "000000000000000000",
										 17 => "000000000000000000",
										 18 => "000000000000000000",
										 19 => "000000000000000000",
										 20 => "000000000000000000",
										 21 => "000000000000000000",
										 22 => "000000000000000000",
										 23 => "000000000000000000",
										 24 => "000000000000000000",
										 25 => "000000000000000000",
										 26 => "000000000000000000",
										 27 => "000000000000000000",
										 28 => "000000000000000000",
										 29 => "000000000000000000",
										 30 => "000000000000000000",
										 31 => "000000000000000000"
										 );
	-- current instruction signal										 
	signal instruction : std_logic_vector(17 downto 0);
	-- alias for decoding instruction
	alias mode : std_logic is instruction(17); -- mode '0' normal, '1' branch
	alias condition : std_logic_vector(1 downto 0) is instruction(16 downto 15); -- condition for branching
	alias cond_val : std_logic is instruction(14); -- condition value to test on flag
	
begin

	-- decoding instruction (unpacked instruction -- easy)
	alu_op <= instruction(16 downto 15); 
	fld_a <= instruction(14 downto 12);
	fld_b <= instruction(11 downto 9);
	fld_c <= instruction(8 downto 6);
	-- computing control signal base on instruction
	ld_reg <= (instruction(5) and not mode); -- write back condition
	ld_x <= (instruction(4) and not mode); -- register input condition
	ld_z <= (instruction(3) and not mode); -- register output
	sel_x <= (instruction(2) and not mode); -- input x selection
	
	-- reading instruction from ROM
	instruction <= rom(conv_integer(csar));
	
	-- address generator and done signal
	-- process for computing next address and computing done signal
	process(clk)
	begin
		if (clk'event and clk='1') then
			if (rst_n = '0') then  -- reseting machine
				csar <= "000000";
				done <= '1';
			else
				-- done computation
				if (mode = '0') then
					if (instruction(0) = '1') then
						done <= '0';
					elsif (instruction(1) = '1') then
						done <= '1';
					end if;
				end if;
				
				-- csar
				 -- csar <= csar + 1; (valor default) when not a branch
				if (mode = '1') then
					-- mode 1 so should evaluate branch condition
					if ((condition = "11"  and cy = cond_val) or
						(condition = "10"  and neg = cond_val) or	
						(condition = "01" and zr = cond_val) or	
						(condition = "00" and start = cond_val)) then
						-- branch taken
						csar <= instruction(5 downto 0);
					else
						-- branch not taken
						csar <= csar + 1;
					end if;
				else
					-- not a branch instruction
					-- so, go to next instruction
					csar <= csar + 1; -- default value
				end if;
			end if;
		end if;
	end process;

end rtl;
