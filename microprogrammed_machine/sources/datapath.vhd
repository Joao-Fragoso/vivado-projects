----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/20/2019 13:30:00 PM
-- Design Name: datapath
-- Module Name: datapath - rtl
-- Project Name: microprogrammed_machine
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: Microprogrammed controller with datapath.
--              Based on chap 14 - Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to chapter 14.
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all; -- for add/sub operation
use ieee.std_logic_arith.all; -- for converting std_logic_vector into integer

entity datapath is
    port (
        clk    : in  std_logic;
        x      : in  std_logic_vector (7 downto 0);
        z      : out std_logic_vector (7 downto 0);
        fld_a  : in  std_logic_vector (2 downto 0);
        fld_b  : in  std_logic_vector (2 downto 0);
        fld_c  : in  std_logic_vector (2 downto 0);
        sel_x  : in  std_logic;
        ld_x   : in  std_logic;
        ld_z   : in  std_logic;
        ld_reg : in  std_logic;
        alu_op : in  std_logic_vector (1 downto 0);
        cy     : out std_logic;
        neg    : out std_logic;
        zr     : out std_logic
    );
end datapath;

architecture rtl of datapath is
    -- type declaration for further register file declaration
    type reg_bank_type is array (natural range <>) of std_logic_vector(7 downto 0);
    -- register file declaration -- initialize to dummy values
    signal register_file : reg_bank_type(0 to 7) := ( x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF");
    -- ALU result signal
    signal alu_out : std_logic_vector(7 downto 0);
    -- internal bus C signal
    signal C : std_logic_vector(7 downto 0);
    -- internal bus A signal - 1st ALU operator
    signal A : std_logic_vector(7 downto 0);
    -- internal bus B signal - 2st ALU operator
    signal B : std_logic_vector(7 downto 0);
    -- register for input X
    signal reg_in : std_logic_vector(7 downto 0);
    -- temporary signals for computing ALU flags
	signal szr : std_logic;
	signal scy : std_logic;
	alias sneg : std_logic is alu_out(7); -- declaring most signficant bit as signal flag
    
begin

    -- multiplexer on bus C
    bus_C : C <= reg_in when sel_x = '1' else alu_out;
    -- multiplexer on bus A from register file
    bus_A : A <= register_file(conv_integer(fld_a));
    -- multiplexer on bus B from register file
    bus_B : B <= register_file(conv_integer(fld_b));
    

    ALU : alu_out <= A + 1   when alu_op = "11" else  -- incremento
               A xor B when alu_op = "10" else  -- xor
               A - B   when alu_op = "01" else  -- sub
               A + B; -- adicao
               
    zero_detection : szr <= '1' when alu_out = x"00" else '0';
    carry_out : scy <= '1' when (alu_op(1) = '0' and -- alu add/sub
                                ((A(7)='1' and B(7)='1') or -- carry out computation
                                ((A(7)='1' or B(7)='1') and alu_out(7)='0'))) else 
                       '0';
    
    -- process for creating datapath registers
    registers: process(clk)
    begin
        if (clk'event and clk='1') then
			-- register flags
			zr <= szr;
			neg <=  sneg;
			cy <= scy;
            -- register_file
            if (ld_reg = '1') then
                register_file(conv_integer(fld_c)) <= C;
            end if;
            -- input X register
            if (ld_x = '1') then
                reg_in <= x;
            end if; 
            -- output Z register
            if (ld_z = '1') then
                z <= alu_out;
            end if;
        end if;
    end process;
    
end rtl;
