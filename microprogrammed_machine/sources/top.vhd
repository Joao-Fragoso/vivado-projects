----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/20/2019 13:30:00 PM
-- Design Name: top
-- Module Name: top - rtl
-- Project Name: microprogrammed_machine
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: Microprogrammed controller with datapath.
--              Based on chap 14 - Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to chapter 14.
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;

entity top is
    port (
        clk   : in  std_logic; -- clock signal
        start : in  std_logic; -- start (active high) start the machine
        rst_n : in  std_logic; -- reset (active low) global
        x     : in  std_logic_vector (7 downto 0);  -- input data
        z     : out std_logic_vector (7 downto 0); -- output data
       done   : out std_logic  -- programs ends signal
    );
end top;

architecture rtl of top is

    -- control unit to datapath signals
    signal fld_a  : std_logic_vector (2 downto 0); -- instruction field A (operator A register)
    signal fld_b  : std_logic_vector (2 downto 0); -- instruction field B (operator B register)
    signal fld_c  : std_logic_vector (2 downto 0); -- instruction field C (target register)
    signal sel_x  : std_logic; -- sel input X origin
    signal ld_x   : std_logic; -- enable input X register
    signal ld_z   : std_logic; -- enable output Z register
    signal ld_reg : std_logic; -- enable write back into register file
    signal alu_op : std_logic_vector (1 downto 0);  -- alu operation selection
                                                    -- "11" -> increment
                                                    -- "10" -> xor
                                                    -- "01" -> subtraction
                                                    -- "00" -> addition
    -- datapath to control unit signal
    signal cy     : std_logic;  -- carry out flag
    signal neg    : std_logic;  -- negative result flag
    signal zr     : std_logic; -- zero result flag

-- component declarations
    component control_unit
    port ( 
        clk    : in  std_logic;
        rst_n  : in  std_logic;
        start  : in  std_logic;
        done   : out std_logic;
        fld_a  : out std_logic_vector (2 downto 0);
        fld_b  : out std_logic_vector (2 downto 0);
        fld_c  : out std_logic_vector (2 downto 0);
        sel_x  : out std_logic;
        ld_x   : out std_logic;
        ld_z   : out std_logic;
        ld_reg : out std_logic;
        alu_op : out std_logic_vector (1 downto 0);
        cy     : in  std_logic;
        neg    : in std_logic;
        zr     : in  std_logic
        );
    end component control_unit;

    component datapath
    port (
        clk    : in  std_logic;
        x      : in  std_logic_vector (7 downto 0);
        z      : out std_logic_vector (7 downto 0);
        fld_a  : in  std_logic_vector (2 downto 0);
        fld_b  : in  std_logic_vector (2 downto 0);
        fld_c  : in  std_logic_vector (2 downto 0);
        sel_x  : in  std_logic;
        ld_x   : in  std_logic;
        ld_z   : in  std_logic;
        ld_reg : in  std_logic;
        alu_op : in  std_logic_vector (1 downto 0);
        cy     : out std_logic;
        neg    : out std_logic;
        zr     : out std_logic
        );
    end component datapath;


begin
    -- datapath instantiation
    i_datapath : datapath port map (
        clk    => clk,
        x      => x,
        z      => z,
        fld_a  => fld_a,
        fld_b  => fld_b,
        fld_c  => fld_c,
        sel_x  => sel_x,
        ld_x   => ld_x,
        ld_z   => ld_z,
        ld_reg => ld_reg,
        alu_op => alu_op,
        cy     => cy,
        neg    => neg,
        zr     => zr);

    -- control unit instatiation
    i_control : control_unit port map (
        clk    => clk,
        rst_n  => rst_n,
        start  => start,
        done   => done,
        fld_a  => fld_a,
        fld_b  => fld_b,
        fld_c  => fld_c,
        sel_x  => sel_x,
        ld_x   => ld_x,
        ld_z   => ld_z,
        ld_reg => ld_reg,
        alu_op => alu_op,
        cy     => cy,
        neg    => neg,
        zr     => zr);

end rtl;
