----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/12/2019 13:30:00 PM
-- Design Name: serial multiplier
-- Module Name: tb_serial_multiplier - rtl
-- Project Name: Serial Multiplier
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: Tesbench for a serial multiplier for positive integers.
--              Based on section 13.7 of Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to book
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--empty testbench entity
entity tb_serial_multiplier is
end tb_serial_multiplier;

architecture behavioral of tb_serial_multiplier is

	--component declaration
	component serial_multiplier
	port ( 
		clk   : in  std_logic; -- clock signal
		rst_n : in  std_logic; -- reset (active low) for state machine
		x     : in  std_logic_vector (7 downto 0); -- first operator
		y     : in  std_logic_vector (7 downto 0); -- second operator
		z     : out std_logic_vector (15 downto 0); -- multiplication result
		start : in  std_logic;  -- start (active high) for new computation
		busy  : out std_logic  -- busy (active high) signal. computation is going on
	);
	end component serial_multiplier;
	
	--testbench signals
	signal clk   : std_logic := '0';
	signal rst_n : std_logic := '0';
	signal x     : std_logic_vector (7 downto 0);
	signal y     : std_logic_vector (7 downto 0);
	signal z     : std_logic_vector (15 downto 0);
	signal start : std_logic := '0';
	signal busy  : std_logic;
	-- testbench check signals
	signal expected : std_logic_vector(15 downto 0);
	type CHECK_TYPE is (WAITING, OK, FAIL);
	signal check : CHECK_TYPE;

begin

	clock : clk <= not clk after 5 ns; -- 10ns period clock

	-- component instatiation
	multiplier : serial_multiplier
	port map (
		clk   => clk,
		rst_n => rst_n,
		x     => x,
		y     => y,
		z     => z,
		busy  => busy,
		start => start
	);
			
	-- process for test signals generation
	tb : process
	begin
		-- reseting machine (rst_n starts asserted)
		wait for 25 ns;
		-- de-assert reset signal
		rst_n <= '1';
		-- test IDLE state before starting (two clock cycles)
		wait until clk'event and clk = '1';
		wait until clk'event and clk = '1';

		-- start a computation
		start <= '1';
		x <= x"BC";
		y <= x"CB";
		expected <= x"9514";

		-- wait until busy is asserted
		wait until busy = '1';
		-- de-assert start signal
		start <= '0';
		-- wait for completion
		wait until busy = '0';
		-- synchronize on next clock cycle
		wait until clk'event and clk='1';

		-- start a new test
		start <= '1';
		x <= x"23";
		y <= x"54";
		expected <= x"0B7C";
	
		-- wait until busy is asserted
		wait until busy = '1';
		-- de-assert start signal
		start <= '0';
		-- wait for completion
		wait until busy = '0';
		-- synchronize on next clock cycle
		wait until clk'event and clk='1';

		-- stop simulation with a failure urghh!!! :(
		assert false report "Simualtion ends!!!" severity failure;	
	end process;

	-- verifying result
	verify : check <= OK   when (start = '0' and busy = '0' and expected = z) else
	                  FAIL when (start = '0' and busy = '0' and expected /= z) else
	                  WAITING;         

end behavioral;

