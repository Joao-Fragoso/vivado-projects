----------------------------------------------------------------------------------
-- Company: Uergs
-- Engineer: Joao Leonardo Fragoso 
-- 
-- Create Date: 06/12/2019 13:30:00 PM
-- Design Name: serial multiplier
-- Module Name: serial_multiplier - rtl
-- Project Name: Serial Multiplier
-- Target Devices: xc7a100tcsg324-3
-- Tool Versions: Vivado 2017.3
-- Description: A serial multiplier for positive integers.
--              Based on section 13.7 of Introduction to Digital System; Ercegovac, Tomas Lang Milos D. Ercegovac (Author)
--              Not intent to Synthesis, olny simulation.
--              For more details, please refer to book
-- Dependencies: None
-- 
-- Revision: 0.02 - Added comments and formating
-- Revision 0.01 - File Created
-- Additional Comments:
--      RUN BEHAVIORAL SIMULATION
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
-- for add operation on std_logic_vectors
use ieee.std_logic_unsigned.all;

entity serial_multiplier is
    port ( 
		clk   : in  std_logic; -- clock signal
        rst_n : in  std_logic; -- reset (active low) for state machine
        x     : in  std_logic_vector (7 downto 0); -- first operator
        y     : in  std_logic_vector (7 downto 0); -- second operator
        z     : out std_logic_vector (15 downto 0); -- multiplication result
        start : in  std_logic;  -- start (active high) for new computation
		busy  : out std_logic  -- busy (active high) signal. computation is going on
	);
end serial_multiplier;

architecture rtl of serial_multiplier is
	-- datapath signals
	signal x_reg    : std_logic_vector(7 downto 0);  -- input X register
	signal y_reg    : std_logic_vector(7 downto 0);  -- input Y register
	signal z_reg    : std_logic_vector(15 downto 0); -- output Z register (VHDL'94 does not allow reading output)
	signal x_and_y0 : std_logic_vector(7 downto 0);  -- result of X and y(0) 
	signal x_plus_z : std_logic_vector(8 downto 0);  -- result of X + Z(15 downto 8)
													 -- one bit wider to keep carry result
	-- control signals
	signal ld_z     : std_logic; -- enable Z register
	signal ld_x     : std_logic; -- enable X register
	signal ld_y     : std_logic; -- enable Y register
	signal clr_z    : std_logic; -- clear Z register (set Z register to zero)
	signal sh_y     : std_logic; -- enable Y register to shift one bit right
	-- type for states unumeration
	type state_type is ( IDLE, -- reset/initial state (wait for start signal)
						 SETUP, -- load inputs and clear Z (after start signal)
						 C0, -- multiply for bit 0 of y
						 C1, -- multiply for bit 1 of y
						 C2, -- multiply for bit 2 of y
						 C3, -- multiply for bit 3 of y
						 C4, -- multiply for bit 4 of y
						 C5, -- multiply for bit 5 of y
						 C6, -- multiply for bit 6 of y
						 C7  -- multiply for bit 7 of y. computation ends
						 );

	signal current_state : state_type; -- FSM current state
	
begin
	-- process for registering input X
	register_x : process(clk)
	begin
		if (clk'event and clk='1') then
			if (ld_x = '1') then
				x_reg <= x; -- load input X
			end if;
		end if;
	end process;
	
	-- process for registering input y
	-- allow one shift right when sh_y is asserted
	register_y : process(clk)
	begin
		if (clk'event and clk = '1') then
			if (ld_y = '1') then
				y_reg <= y; -- load input Y
			elsif (sh_y = '1') then
				y_reg <= "0" & y_reg(7 downto 1); -- shift right
			end if;
		end if;
	end process;
	
	-- process for registering current Z value
	-- allow clear when clr_z is asserted
	register_z : process(clk)
	begin
		if (clk'event and clk = '1') then
			if (clr_z = '1') then
				z_reg <= (others => '0'); -- clear register
			elsif (ld_z = '1') then
				z_reg <= x_plus_z & z_reg(7 downto 1); -- register accumulation result
			end if;
		end if;
	end process;
	
	-- and of X with y(0)
	and_y : x_and_y0 <= x when y_reg(0) = '1' else (others => '0');
	-- add with one bit extension to save carry
	add_x_z : x_plus_z <= ("0"&x_and_y0) + ("0"&z_reg(15 downto 8));
	
	-- Finite State Machine transitions computation
	fsm : process(clk)
	begin
		if (clk'event and clk = '1') then
			if (rst_n = '0') then
				-- synchronous reset (active low)
				current_state <= IDLE; -- set initial states
			else
				case current_state is
				when SETUP =>
					current_state <= C0;
				when C0 =>
					current_state <= C1;
				when C1 =>
					current_state <= C2;
				when C2 =>
					current_state <= C3;
				when C3 =>
					current_state <= C4;
				when C4 =>
					current_state <= C5;
				when C5 =>
					current_state <= C6;
				when C6 =>
					current_state <= C7;
				when C7 =>
					current_state <= IDLE;
				when others => -- IDLE state
					-- wait for start becames 1
					if (start = '1') then
						current_state <= SETUP;
					end if;
				end case;						
			end if; -- rst
		end if; -- clk
	end process;

	-- control signals computation
	busy <= '0' when current_state = IDLE else '1';
	ld_x <= '1' when current_state = SETUP else '0';
	ld_y <= '1' when current_state = SETUP else '0';
	clr_z <= '1' when current_state = SETUP else '0';
	sh_y <= '0' when (current_state = IDLE or current_state = SETUP) else '1';
	ld_z <= '0' when (current_state = IDLE or current_state = SETUP) else '1';

	-- output assignement (Vhdl'94)
	z <= z_reg;
	
end rtl;
